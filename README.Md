## Summary:

The program **pyEvent** is a package of different programs achieving data extraction corresponding to a list of events at different stations/networks.

### Description:

* **00_get_stations.py** = build a list of stations from different networks. The file 00_get_stations.py has to be filled in by the user before launching the program.

* **05_get_events.py** = build a catalog of events. The file 05_get_events.py has to be filled in by the user before launching the program. 

* **06_get_extractions.py** = extract the waveforms corresponding to the list of events at the different stations, process the waveform, convert and store them. The file 06_get_extractions.py has to be filled in by the user before launching the program.

### Installation

The program needs Python 3 at least, ObsPy and several libraries.

Python can be installed in a virtual environment, using Anaconda as follow:

```
wget https://repo.continuum.io/archive/Anaconda3-5.3.1-Linux-x86_64.sh

chmod u+x  Anaconda2-4.4.0-Linux-x86_64.sh
./Anaconda2-4.4.0-Linux-x86_64.sh
-> say yes to export PATH to $HOME/.bashrc
source .bashrc
conda config --add channels conda-forge
conda create -n mypython python=3.7 
source activate mypython
conda install obspy
conda install h5py
conda install cartopy
conda install ipython
conda install matplotlib

vi $HOME/.bash_profile
export PYTHONPATH=« /$pyEvent_Path/pyEvent_public:$PYTHONPATH"

source activate mypython 
source deactivate
```


### Usage
The programs needs 5 mandatories inputs in the following order:
* the directory containing the extracted data defined in 06_get_extractions.py.
* the logs directory defined in 06_get_extractions.py.
* the nodata file defined in 06_get_extractions.py.


The program can extracts data from:
* FDSNWS protocol open access 
* restricted FDSNWS protocol with login/pwd
* EIDA Token Authentication Service

**Example**:
```
00_get_stations.py
05_get_events.py
06_get_extractions.py

```
