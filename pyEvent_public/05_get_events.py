#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
:author:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)

:copyright:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)

:license:
	GPL-3.0-or-later

Parameters for the get events program. Events informations will be dowloaded and stored in a catalog file.
Event catalogs can be created from:
- IRIS fdsn ws event
- INGV fdsn ws event

Exemple:
	05_get_events.py
 
"""

import pyEvent.m50_get_events as get_evts
from obspy.core import UTCDateTime
in_                   = {}

###############
# Event catalog using iris.edu fdsnws event
###############  
# Here you'll find all possible options and their format 
# =====>    http://http://service.iris.edu/fdsnws/event/1/
# OR
# =====>    http://webservices.ingv.it/fdsnws/event/1/
#in_['key']        = value 
dc = 'INGV' # datacenter to query for fdsn ws event. Available dc = IRIS or INGV

in_['start']           = '2018-09-01' # yyyy-mm-dd
in_['end']             = '2018-10-15' # yyyy-mm-dd
in_['minlat']          = '20' # min lat
in_['maxlat']          = '50' # max lat
in_['minlon']          = '2' # min lon
in_['maxlon']          = '40' # max lon
in_['minmag']          = '1.0' # min magnitude
#in_['catalog']         = 'NEIC PDE' # If IRIS, catalogs available = NEIC PDE, ISC (UK), GCMT. If other dc, please comment this line
#in_['maxmag']          = '5.5' # max magnitude
# ...

evtfile                = 'event' # output name (.txt and .png)

############### RUN
get_evts.find_events(dc,input_user=in_,eventfile=evtfile)

