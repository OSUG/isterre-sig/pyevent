#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
:author:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)

:copyright:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)

:license:
  GPL-3.0-or-later

 
"""


# Libraries
from obspy.core import read, Trace, Stream, UTCDateTime
import obspy.io.sac
import copy
import numpy as np
import obspy.signal
import scipy.signal
import matplotlib.pyplot as plt
import os
import glob
import h5py as h5 
import pickle # cpickle bizarre ajouter option 2?
from os.path import basename, dirname
from obspy.io.sac import SACTrace
from datetime import datetime
from time import mktime
from obspy.core import AttribDict

from obspy.core import AttribDict

import obspy.clients.fdsn
from obspy.clients.fdsn import RoutingClient
from obspy.clients.arclink.client import Client as arclink_client
import pyEvent.modules.lang as lang
import pyEvent.modules.dd as dd

from obspy.taup.taup_geo import calc_dist
from obspy.taup import TauPyModel



def download_extractions(nbsta,input_user={},sta_list={},evt_list={}):
	''' download extraction data corresponding to event listed in the event file. 
	If a lock file is found in the data_path/event_name directory, goes to the next event (event downloading and under process)
	Check if the trace has already been downloaded before
	If not, create a lock file
	Download the data
	Data Processing
	Write data as SAC file with header filled OR as a miniseed file:
	event directory name: 2018-09-18T21:36:43
	Sac file name: 2018-09-18T21-36-43.FR.OG35.00.HHZ.D.SAC
	Miniseed file name: 2018-09-18T21-36-43.FR.OG35.00.HHZ.D.mseed

	sac OUTPUT files : 
	- Sac files data recorded in the data_path/event_name (YYY-MM-DDTHH-MM-SS)
	- Sac header is filled af follow:
		stla : station latitude
		stlo : station longitude
		stel : station elevation
		evla : event latitude
		evlo : event longitude
		evdp : event depth
		user0 : event magnitude
		a    : first arrival time in sec
		ka   : First arrival identification
		cmpinc=st0[0][0].channels[0].dip+90    # dip + 90, = incidence angle (degrees, from vertical)
		cmpaz=st0[0][0].channels[0].azimuth    #Component azimuth angle (degrees, from Geographic North)

	hdf5 output file structure:
	Event_name.h5/$net/$sta.$locid/$chan/data (dataset trace)
										/cmpdip  Component dip angle (degrees, from horizontal)
										/cmpaz Component azimuth angle (degrees, from Geographic North)
								  /station/phase
								  		  /stel
								  		  /stla
								  		  /stlo
								  		  /tp
				 /_metadata/fe
				 /_metadata/event/ev_date
				 				 /evdp
				 				 /evla
				 				 /evlo
				 				 /mag 

	Processing:
	manage gaps and multi traces in stream
	demean
	detrend
	low pass filter (high corner frequency defined by the user)
	downsampling
	if remove_resp=True, remove response
	band pass filter (high and low corner frequency defined by the user)
	Tapered
	Filled with 0 if trace length is smaller than final length
	cut to final length
	'''
# HDF5 : def write_as_hdf5(out_file,st,ista,icmp) et def write_metadata_hdf5(out_file,in_) :

	tr_len = input_user['pp']['tr_len']
	tap_len = input_user['pp']['tap_len']
	pre_event = input_user['pp']['pre_event'] 
	out_dir = input_user['data_path']
	channel=input_user['ch']
	mode=input_user['mode']
	cmp=input_user['cmp']
	data_format=input_user['pp']['data_output']
	logdir=input_user['log_path']
	logfile=input_user['nodatafile']+'_'+datetime.now().strftime("%y-%m-%d-%H%M%S")  
	sta_values = {}                                # dico stations list
	sta_values   = lang.merge_options(sta_values,sta_list) # update dico stations list with station list file
	evt_values = {}                                # dico evt list
	evt_values   = lang.merge_options(evt_values,evt_list) # update dico evt list with evt list file
	file_done_list = []     # liste of file already downloaded, saved in a pickle file per event
	is_pickle = False
	#print evt_values
		
	############ Read event catalog - EVENT LOOP ####################


	for evname in evt_values :
		k=0
		file_done_list = []
		dd.dispc('      Event:'+ evname + '  under process','g','n')
		event_dir = define_eventdir(input_user,evt_values[evname])

		if has_this_event_already_been_processed(evt_values[evname]['EvDate'],out_dir, data_format, event_dir) : continue #next event
		create_lock_file(evt_values[evname]['EvDate'],out_dir, data_format, event_dir) 
		is_pickle, pkl_filename = has_this_event_a_pickle_file(evt_values[evname]['EvDate'],out_dir, data_format, event_dir)
		if is_pickle and mode==1: # completion mode even if extractions had been done for event n, read the pkl list and try again to complete
			dd.dispc('  '+ evt_values[evname]['EvDate'] + ' has a pkl file already - completion mode - try to complete event','r','n')
			pkl_file = open(pkl_filename, 'rb')
			file_done_list = pickle.load(pkl_file,encoding='latin1')
			#print file_done_list
			# pickle.dump( data, open( pkl_filename, "wb" ) )
		elif is_pickle and mode==0: # initial mode if a pkl file is present, the event is done, go next
			dd.dispc('  '+ evt_values[evname]['EvDate'] + ' has a pkl file already - initial mode - go next event','r','n')
			delete_lock_file_2(evt_values[evname]['EvDate'],out_dir, data_format, event_dir)
			continue
		
			############ Read stations list - STATION LOOP ####################
		for staname in sta_values :
			
			k+=1 # Test station number to remove the lock file at the end 
		
			############ Parsing components list Z, N, E ####################
			for icmp in cmp:

				path = define_path(icmp,input_user,evt_values[evname],sta_values[staname])
				if staname+'_'+icmp+'_'+data_format in file_done_list : 
					dd.dispc('      Trace already downloaded !'+ path['file_name']+'_'+data_format,'g','n')
					continue
				
				else:
					st, trace, T1, T2, ccmp = download_data(channel, icmp,input_user['pp'],evt_values[evname],sta_values[staname])
					st0, inv = download_channel(T1,T2,input_user['pp'],ccmp,sta_values[staname])
					if trace:
						st=process_trace(st,inv,T1,T2,input_user['pp'],sta_values[staname])
						if st:
					 		write_evt(st,st0,inv,data_format,ccmp,path,input_user['pp'],evt_values[evname],sta_values[staname])
					 		file_done_list.append(staname+'_'+icmp+'_'+data_format) # add to list
					 		dd.dispc('Successful download ! \t' + evname + '\t' + staname+'_'+ccmp+'_'+data_format,'g','n')
						
					elif (st==0 or trace==0): # No trace available to download - create record in the log file
						dd.dispc('No trace for evt= \t' + evname + '  station  '+ staname+'_'+ccmp,'g','n')
						try: # Create data directory evt directory name YYYY-MM-DDThh:mm:ss
							os.stat(logdir)
						except:
							os.mkdir(logdir)
						ff = open(logfile + '.txt','a')
						ff.write(evname +'\t'+ staname+'_'+icmp+'\n')
						ff.close
		delete_lock_file(k,nbsta,evt_values[evname]['EvDate'],out_dir, data_format, event_dir)
		pickle.dump( file_done_list, open( pkl_filename, "wb" ) )
##		
########################################################################################
##########################################################################################
#########           #####         ####               #####           #####################
#########           ####          ####               ####     ############################
#########     ##########     ##############     #########     ############################
#########     #########     ###############     ##########        ########################
#########        ######     ###############     ##############       #####################
#########     #########      ##############     ##################    ####################
#########     ##########          #########     #################     ####################
#########     ###########         #########     ##########           #####################
##########################################################################################
##########################################################################################


#---------------------------------------------------------------------------------------
#-------------------- CONFIGURE SUBFUNCTIONSFUNCTIONS ----------------------------------
#---------------------------------------------------------------------------------------

def has_this_event_already_been_processed(EvDate,out_dir, data_format, event_dir):

	if ((data_format=='SAC' or data_format=='MINISEED') and os.path.isfile(out_dir+'/'+event_dir+'/'+EvDate+".lock")) : # paralle processing
		done=True
	elif ((data_format=='HDF5') and os.path.isfile(out_dir+'/Events'+'/'+EvDate+".lock")):
		done=True
	else :
		done=False

	if done == True  : dd.dispc('  '+EvDate + ' is already (being) dowloaded','r','n')
	if done == False : dd.dispc('  '+EvDate + ': lets go','y','b')
	return done 

def has_this_event_a_pickle_file(EvDate,out_dir, data_format, event_dir):
	 
	if ((data_format=='SAC' or data_format=='MINISEED') and os.path.isfile(out_dir+'/'+event_dir+'/'+EvDate+'-'+data_format+".pkl")) : # paralle processing
		is_pickle=True
	elif ((data_format=='HDF5') and os.path.isfile(out_dir+'/Events'+'/'+EvDate+'-'+data_format+".pkl")):
		is_pickle=True
	else :
		is_pickle=False

	#if is_pickle==True  : dd.dispc('  '+EvDate + ' has a pkl file already','r','n')
	#if is_pickle==False : dd.dispc('  '+EvDate + ': No pkl file','y','b')
	if (data_format=='HDF5'):
		pkl_file=out_dir+'/Events'+'/'+EvDate+'-'+data_format+".pkl"
	else:
		pkl_file=out_dir+'/'+event_dir+'/'+EvDate+'-'+data_format+".pkl"
	return is_pickle, pkl_file 

def create_lock_file(EvDate, outdir, data_format, event_dir) :
	if (data_format=='HDF5'):
		lock_dir=outdir+'/Events'
	else:
		lock_dir=outdir+'/'+event_dir
	try: # Create data directory evt directory name YYYY-MM-DDThh-mm-ss
			os.stat(lock_dir)
	except:
			os.mkdir(lock_dir)
	lock_file=lock_dir+'/'+EvDate+".lock"
	ff=open(lock_file,'w')
	ff.close()
	
#------------------------------------------------------
	
def delete_lock_file(sta_nb,nbrsta,EvDate, outdir, data_format, event_dir):

	if (data_format=='HDF5'):
		lock_dir=outdir+'/Events'
	else:
		lock_dir=outdir+'/'+event_dir

	if(sta_nb==nbrsta):
		os.remove(lock_dir+'/'+EvDate+".lock")


def delete_lock_file_2(EvDate, outdir, data_format, event_dir):
	
	if (data_format=='HDF5'):
		lock_dir=outdir+'/Events'
	else:
		lock_dir=outdir+'/'+event_dir

	os.remove(lock_dir+'/'+EvDate+".lock")
				
#------------------------------------------------------
				
def read_station_list(stationfile='station') :
	sta={}
	ff=open(stationfile + '.txt','r')
	nbsta=0
	for iline in ff : 
		(dc,net,name,loc,lat,lon,elev,depth) = iline.split()
		if loc=='--' : loc = ''
		if loc == u'' : kname=net+'_'+name+'_00'
		else: kname=net+'_'+name+'_'+loc 
		sta[kname]={}
		sta[kname]['net']  = net
		sta[kname]['name'] = name
		sta[kname]['loc']  = loc
		sta[kname]['kname']= kname
		sta[kname]['lat']  = lat
		sta[kname]['lon']  = lon
		sta[kname]['elev'] = elev
		sta[kname]['depth']= depth
		sta[kname]['dc']   = dc
		nbsta+=1
	ff.close()
	return sta, nbsta 

#------------------------------------------------------
def read_event_list(eventfile='event') :
	evt={}
	ff=open(eventfile + '.txt','r')
	for iline in ff : 
		(EvDate,latev,lonev,prof,mag) = iline.split()
		Evname= EvDate
		evt[Evname]={}
		evt[Evname]['EvDate']  = EvDate #YYY-MM-DDThh:mm:ss
		evt[Evname]['lat'] = latev
		evt[Evname]['lon']  = lonev
		evt[Evname]['prof']= prof
		evt[Evname]['mag']  = mag
		(EvDate_1,EvDate_2) = evt[Evname]['EvDate'].split('T')
		
		(year,month,day) = EvDate_1.split('-')
		(hour,minute,sec) = EvDate_2.split(':')
		evt[Evname]['year']  = year
		evt[Evname]['month']= month
		evt[Evname]['day']  = day
		evt[Evname]['hour']  = hour
		evt[Evname]['minute']= minute
		evt[Evname]['sec']  = sec
	ff.close()
	return evt

#________________ Download metadata level = channel---------------------------

def download_channel(t1,t2,pp,component,sta_list={}):
	db = dict()
	db['sta'] = copy.deepcopy(sta_list) # sta parameters
	net_list=pp['net_token']
	token=pp['eida_token']
	st0=0
	inv=0

	if ((db['sta']['net'],db['sta']['dc']) in net_list): # EIDA Token
		try:
			rsClient = RoutingClient("eida-routing", credentials={'EIDA_TOKEN': str(token)})
		except:
			inv=0
		try:
			inv = rsClient.get_stations(starttime=t1, endtime=t2,network=db['sta']['net'], station=db['sta']['name'], loc=db['sta']['loc'], channel=component,level="response")
		except:
			inv=0


	else:
		try:
			client = obspy.clients.fdsn.Client(db['sta']['dc'])
		except:
			st0=0
		try:
			st0 =client.get_stations(starttime=t1, endtime=t2, network=db['sta']['net'], station=db['sta']['name'], channel=component,level='channel')
		except:
			st0=0


	return st0, inv 



#________________ Download trace---------------------------


def download_data(channel,comp,pp,evt_list={},sta_list={}):

	db = dict()
	db['evt'] = copy.deepcopy(evt_list) # evt parameters
	db['sta'] = copy.deepcopy(sta_list) # sta parameters
	tap_len = pp['tap_len']
	tr_len = pp['tr_len']
	pre_event = pp['pre_event']  
	username=pp['login']
	pwd=pp['pwd']
	net_list=pp['net_token']
	token=pp['eida_token']
	net_list2=pp['net_fdsnws']  
	
	T_evt=UTCDateTime(int(db['evt']['year']),int(db['evt']['month']),int(db['evt']['day']),int(db['evt']['hour']),int(db['evt']['minute']),int(db['evt']['sec']))
	T_evt=T_evt-pre_event
	T1=T_evt-tap_len
	T2=T1+tr_len+2*tap_len
	
	if ((db['sta']['net'],db['sta']['dc']) in net_list): # EIDA Token
		#print("Z3 => EIDA Token")
		try:
			rsClient = RoutingClient("eida-routing", credentials={'EIDA_TOKEN': str(token)}) 	# 		client.max_status_requests = 200
		except:
	 		trace=0
	 		dd.dispc('  data center ' + db['sta']['dc'] + ' cannot be reached','r','n')
		for ich in channel: #Try the channel list : BH then HH if necessary
	 		ccmp= ich + comp
	 		dd.dispc('      attempting to download  '+db['sta']['name']+'_'+ccmp,'c','d')
	 		try  :
	 			
	 			st = rsClient.get_waveforms(network=db['sta']['net'], station=db['sta']['name'], location=db['sta']['loc'], channel=ccmp, starttime=T1, endtime=T2)
	 			#print("st:",st)
	 			if len(st): # Exception wether st returned is null
	 				trace=1
	 				break
	 			else:
	 				trace=0
	 				continue

	 		except :
	 			trace=0
	 			st = 0
	 			continue


	else: # fdsnws with login/pwd
		if ((db['sta']['net'],db['sta']['dc']) in net_list2):
			#print(" XT => fdsn ws avec passwd")
			try:
				client = obspy.clients.fdsn.Client(db['sta']['dc'], user=username, password=pwd)
			except:
				trace=0
			for ich in channel : #Try the channel list : BH then HH if necessary
				ccmp= ich + comp
				dd.dispc('      attempting to download  '+db['sta']['name']+'_'+ccmp,'c','d')
				try  :
					st = client.get_waveforms(db['sta']['net'],db['sta']['name'],db['sta']['loc'],ccmp,T1,T2,attach_response=True)
					if len(st): # Exception wether st returned is null
						trace=1
						break
					else:
						trace=0
						continue
					
				except :
					trace=0
					st = 0
					continue
		else: # fdsnws 
			#print(" ni XT ni Z3 => fdsn ws sans passwd")
			try:
				client = obspy.clients.fdsn.Client(db['sta']['dc'])
			except:
				trace=0
			for ich in channel : #Try the channel list : BH then HH if necessary
				ccmp= ich + comp
				dd.dispc('      attempting to download  '+db['sta']['name']+'_'+ccmp,'c','d')
				try  :
					st = client.get_waveforms(db['sta']['net'],db['sta']['name'],db['sta']['loc'],ccmp,T1,T2,attach_response=True)
					if len(st): # Exception wether st returned is null
						trace=1
						break
					else:
						trace=0
						continue
				except :
					trace=0
					st = 0
					continue			
	return st, trace, T1, T2, ccmp
								




#________________ Write sac trace------------------------------------
###   sac headers filling
### write trace as a sac or miniseed file in /SAC/EVENT/ directory (EVENT=YYYY-MM-DDThh:mm:ss)

def write_evt(st,st0,inv,data_format,chan,path,pp,evt_list={},sta_list={}) :
	ev_ = dict()
	db = dict()
	db['evt'] = copy.deepcopy(evt_list) # evt parameters
	db['sta'] = copy.deepcopy(sta_list) # sta parameters
	pre_event = pp['pre_event']

	## Given the source and receiver location, calculate the azimuth and distance and time of the first arrival tp
	dist = calc_dist(float(db['evt']['lat']), float(db['evt']['lon']),float(db['sta']['lat']),float(db['sta']['lon']),6371,0)
	try:
		model = TauPyModel(model="iasp91")
		arr = model.get_travel_times(float(db['evt']['prof']),dist)
		tp=arr[0].time+pre_event
		phase=arr[0].name
	except:
		tp=0
		phase='P'
		
	##origin time of event in the eventfile - do not take in account msec

	ev_['date']=db['evt']['EvDate'].split('T')[0]
	ev_['time']=db['evt']['EvDate'].split('T')[1]
	ev_['year']=ev_['date'].split('-')[0]
	ev_['month']=ev_['date'].split('-')[1]
	ev_['day']=ev_['date'].split('-')[2]
	ev_['hour']=ev_['time'].split(':')[0]
	ev_['minute']=ev_['time'].split(':')[1]
	ev_['sec']=ev_['time'].split(':')[2]

	ev_['sec1']=int(round(float(ev_['sec'])))
	if(ev_['sec1']==60):
		ev_['sec1']=0
		ev_['minute']=int(ev_['minute'])+1
	if ev_['minute']==60:
		ev_['minute']=0
		ev_['hour']=int(ev_['hour'])+1
	t01=datetime(int(ev_['year']), int(ev_['month']), int(ev_['day']), int(ev_['hour']), int(ev_['minute']), int(ev_['sec1']))
	t0=mktime(t01.timetuple())
	s='t01 : ' + str(t01)+' t0 : ' + str(t0)

	if(data_format=='SAC'):
	#### Create sac file headers ######
		try: # Create data directory evt directory name YYYY-MM-DDThh:mm:ss
			os.stat(path['evt_file'])
		except:
			os.mkdir(path['evt_file'])
		sacd=AttribDict()
		sacd.stla=db['sta']['lat']
		sacd.stlo=db['sta']['lon']
		sacd.stel=db['sta']['elev']
		sacd.evla=db['evt']['lat']
		sacd.evlo=db['evt']['lon']
		sacd.evdp=db['evt']['prof']
		sacd.user0=db['evt']['mag']
		
		if st0:
			sacd.cmpinc=st0[0][0].channels[0].dip+90    # dip + 90, = incidence angle Component incident angle (degrees, from vertical)
			sacd.cmpaz=st0[0][0].channels[0].azimuth #Component azimuth angle (degrees, from Geographic North)
		else:
			cha=inv.get_channel_metadata(db['sta']['net']+"."+db['sta']['name']+"."+db['sta']['loc']+"."+chan, db['evt']['EvDate'])
			sacd.cmpinc=cha['dip']+90
			sacd.cmpaz=cha['azimuth']
		sacd.a=tp
		sacd.ka=phase #First arrival time identification
		st[0].stats.sac=sacd
		sac_name = db['sta']['net']+"."+db['sta']['name']+"."+db['sta']['loc']+"."+chan+".D.SAC"
		
		st.write(path['evt_file']+'/'+sac_name, format='SAC')
		st=read(path['evt_file']+'/'+sac_name) #read sac file to access nztime

		##Time of the first sample = KZTIME do not admit msec round
		sac=SACTrace.from_obspy_trace(st[0])
		# Temps origine of event = time of the first sample
		nzyear=sac.nzyear
		nzjday=sac.nzjday 
		nzhour=sac.nzhour
		nzmin=sac.nzmin
		nzsec=sac.nzsec
		nzmsec=sac.nzmsec
		nzsectot=str(nzsec)+"."+str(nzmsec)
		t1=UTCDateTime(year=nzyear, julday=nzjday, iso8601=True)
		nzmonth=t1.month
		nzday=t1.day
		nzsec1=int(round(float(nzsectot)))
		if nzsec1==60:
			nzsec1=0
			nzmin=nzmin+1
		if nzmin==60:
			nzmin=0
			nzhour=nzhour+1
 
		tref1=datetime(nzyear, nzmonth, nzday, nzhour, nzmin,nzsec1)
		tref=mktime(tref1.timetuple())
		sz='tref1 : ' + str(tref1)+' tref : ' + str(tref)
		sac.o=t0-tref
		sac.write(path['evt_file']+'/'+ev_['year']+"-"+ev_['month']+"-"+ev_['day']+"T"+ev_['hour']+'-'+ev_['minute']+'-'+ev_['sec']+'.'+sac_name) # write final sac file with full header
		os.remove(path['evt_file']+'/'+sac_name)

	elif(data_format=='MINISEED'):
		try: # Create data directory evt directory name YYYY-MM-DDThh:mm:ss
			os.stat(path['evt_file'])
		except:
			os.mkdir(path['evt_file'])
		start_date=str(st[0].stats.starttime).split('T')[0]
		start_time=str(st[0].stats.starttime).split('T')[1]
		start_hour=start_time.split(':')[0]
		start_min=start_time.split(':')[1]
		start_sec1=start_time.split(':')[2] #00.000000Z
		start_sec2=start_sec1.split('.')[0]
		mseed_name = start_date+'T'+start_hour+'-'+start_min+'-'+start_sec2+'.'+db['sta']['net']+"."+db['sta']['name']+"."+db['sta']['loc']+"."+chan+".D.mseed"
		st.write(path['evt_file']+'/'+mseed_name, format='MSEED')

	elif(data_format=='HDF5'):

		write_as_hdf5(st,st0,inv,tp,phase,data_format,chan,path,db['evt'],db['sta']) 



		
#------------------------------------------------------

def write_as_hdf5(st,st0,inv,tp,phase,data_format,chan,path,evt_list={},sta_list={}) :
	db = dict()
	db['evt'] = copy.deepcopy(evt_list) # evt parameters
	db['sta'] = copy.deepcopy(sta_list) # sta parameters

	h5_file_data = path['out_dir']+'/EVENTS'+'/'+db['evt']['year']+"-"+db['evt']['month']+"-"+db['evt']['day']+"T"+db['evt']['hour']+'-'+db['evt']['minute']+'-'+db['evt']['sec']+'.h5'
	fout = h5.File(h5_file_data, "a") 
	locIDold = db['sta']['loc'] 
	if db['sta']['loc'] == u'':
		db['sta']['loc']  = '00'
	dset_name = "/" + db['sta']['net']+"/"+db['sta']['name']+"."+db['sta']['loc']+"/"+chan
	dset_name_sta = "/" + db['sta']['net']+"/"+db['sta']['name']+"."+db['sta']['loc']+"/station"
	trace = np.float32(st[0].data)
	if dset_name in fout : 
		del fout[dset_name]
	dset = fout.create_dataset(dset_name+'/data', data=trace,chunks=True,compression="gzip", compression_opts=9) #dataset trace
	if st0:
		fout.create_dataset(dset_name+'/cmpdip', data=st0[0][0].channels[0].dip+90)    #Component incident angle (degrees, from vertical)
		fout.create_dataset(dset_name+'/cmpaz', data=st0[0][0].channels[0].azimuth) #Component azimuth angle (degrees, from Geographic North)
	else:
		cha=inv.get_channel_metadata(db['sta']['net']+"."+db['sta']['name']+"."+locIDold+"."+chan, db['evt']['EvDate'])
		fout.create_dataset(dset_name+'/cmpdip', data=cha['dip']+90)    #Component incident angle (degrees, from vertical)
		fout.create_dataset(dset_name+'/cmpaz', data=cha['azimuth']) #Component azimuth angle (degrees, from Geographic North)
	if not dset_name_sta in fout : 
		fout.create_dataset(dset_name_sta+'/stla', data=db['sta']['lat'])
		fout.create_dataset(dset_name_sta+'/stlo', data=db['sta']['lon'])
		fout.create_dataset(dset_name_sta+'/stel', data=db['sta']['elev'])
		fout.create_dataset(dset_name_sta+'/tp', data=tp)
		fout.create_dataset(dset_name_sta+'/phase', data=phase)
	if not '/_metadata/fe' in fout : 
		fout.create_dataset('/_metadata/fe', data=st[0].stats.sampling_rate)
	if not '/_metadata/event' in fout : 
		fout.create_dataset('/_metadata/event/evlo', data=db['evt']['lon'])
		fout.create_dataset('/_metadata/event/evla', data=db['evt']['lat'])
		fout.create_dataset('/_metadata/event/evdp', data=db['evt']['prof'])
		fout.create_dataset('/_metadata/event/mag', data=db['evt']['mag'])
		fout.create_dataset('/_metadata/event/ev_date', data=db['evt']['EvDate'])
	fout.close()




#------------------------------------------------------

def define_eventdir(in_,evt_list={}):
	db = dict()
	ev_ = dict()
	db['evt'] = copy.deepcopy(evt_list) # evt parameters

	ev_['date']=db['evt']['EvDate'].split('T')[0]
	ev_['time']=db['evt']['EvDate'].split('T')[1]
	ev_['year']=ev_['date'].split('-')[0]
	ev_['month']=ev_['date'].split('-')[1]
	ev_['day']=ev_['date'].split('-')[2]
	ev_['hour']=ev_['time'].split(':')[0]
	ev_['minute']=ev_['time'].split(':')[1]
	ev_['sec']=ev_['time'].split(':')[2]

	event_dir=ev_['date']+'T'+ev_['hour']+'-'+ev_['minute']+'-'+ev_['sec']

	return event_dir
		
def define_path(icmp,in_,evt_list={},sta_list={}) :
	db = dict()
	ev_ = dict()
	path = {}
	db['evt'] = copy.deepcopy(evt_list) # evt parameters
	db['sta'] = copy.deepcopy(sta_list) # sta parameters
	path                 = {}
	path['out_dir']      = in_['data_path'] # SAC dir
	#path['evt_file']     = path['out_dir']+'/'+db['evt']['EvDate'] # evt_name dir

	ev_['date']=db['evt']['EvDate'].split('T')[0]
	ev_['time']=db['evt']['EvDate'].split('T')[1]
	ev_['year']=ev_['date'].split('-')[0]
	ev_['month']=ev_['date'].split('-')[1]
	ev_['day']=ev_['date'].split('-')[2]
	ev_['hour']=ev_['time'].split(':')[0]
	ev_['minute']=ev_['time'].split(':')[1]
	ev_['sec']=ev_['time'].split(':')[2]

	path['evt_file']     = path['out_dir']+'/'+ev_['date']+'T'+ev_['hour']+'-'+ev_['minute']+'-'+ev_['sec']
	
	path['sac_file']   = path['evt_file']+'/'+ev_['year']+"-"+ev_['month']+"-"+ev_['day']+"T"+ev_['hour']+'-'+ev_['minute']+'-'+ev_['sec']+'.'+db['sta']['net']+"."+db['sta']['name']+"."+db['sta']['loc']+"."+'*'+icmp+".D.SAC" #component BHZ

	path['mseed_file'] = path['evt_file']+'/'+ev_['year']+"-"+ev_['month']+"-"+ev_['day']+"T"+ev_['hour']+'-'+ev_['minute']+'-'+ev_['sec']+'.'+db['sta']['net']+"."+db['sta']['name']+"."+db['sta']['loc']+"."+'*'+icmp+".D.mseed" #component BHZ

	
	path['h5_file'] = path['out_dir']+'/Events/'+ev_['year']+"-"+ev_['month']+"-"+ev_['day']+"T"+ev_['hour']+'-'+ev_['minute']+'-'+ev_['sec']+'.h5'

	path['file_name'] = db['sta']['net']+"."+db['sta']['name']+"."+db['sta']['loc']+"_"+icmp

	return path


def has_this_ch_already_been_dowloaded(h5_filename,icmp,in_,sta_list={}) :
	done = False
	db = dict()
	db['sta'] = copy.deepcopy(sta_list) # sta parameters
	if os.path.isfile(h5_filename) == False :
		done = False
	else :
		f_h5 =h5.File(h5_filename, "r")
		
		for chan in in_['ch']:
			path_in_h5_1='/'+db['sta']['net']+"/"+db['sta']['name']+"."+db['sta']['loc']+"/"+chan+icmp
			
			if path_in_h5_1 in f_h5 :
				done=True
			else :
				done=False
				

		f_h5.close()
	return done


	

#________________ Process_Trace()-------------------------------
### manage gaps and multi traces in stream
### demean
### detrend
### low pass filter (high corner frequency defined by the user)
### downsampling
### if deconvolution, remove response
### band pass filter (high and low corner frequency defined by the user)
### Tapered
### Fill the trace with 0 if smaller than length
### cut to length

def process_trace(st,inv,date1,date2,pp,sta_list={}) :
	db = dict()
	remove_resp       = pp['remove_resp']
	newfreq           = pp['freq'] 
	f_prefilt         = pp['f_prefilt']
	tap_len           = pp['tap_len']
	net_list          = pp['net_token']
	Ltrace            = date2 - date1 # length of the extracted trace
	db['sta'] = copy.deepcopy(sta_list) # sta parameters
	dd.dispc('      Trace under process  ','c','d')
	#print("st:",st)
	st = manage_gaps(st,f_prefilt,Ltrace)

	if st:
		st.detrend(type='constant')
		st.detrend(type='linear')
		st.filter('lowpass',freq=f_prefilt[1],zerophase=True)
		
		rateFreq = float(st[0].stats.sampling_rate)/float(newfreq)
		
		st.decimate(int(rateFreq),no_filter=True)
		tr=st[0]
		L  = float(len(tr)) # length of the extracted trace

		if L/(newfreq*Ltrace) > 1: # case length of extracted trace > final length
			st.merge(method=1,fill_value='interpolate') 
		else:
			st.merge(method=1,fill_value=0) 

		if (remove_resp==False):
			st.taper(100,max_length=tap_len)
			print("No deconvolution")
			st.filter('bandpass',freqmin=f_prefilt[0],freqmax=f_prefilt[1],zerophase=True)
			st.trim(starttime=date1,endtime=date2,fill_value=0,pad=True)
			st.interpolate(newfreq,starttime=date1+tap_len,npts=int(((date2-tap_len)-(date1+tap_len))*newfreq),method='cubic')
			
		#elif (remove_resp and db['sta']['dc']!='RESIF' and db['sta']['net']=='Z3'): ### EIDA TOKEN CASE
		elif (remove_resp and (db['sta']['net'],db['sta']['dc']) in net_list):
			st.remove_response(inventory=inv, output='VEL', taper = False ,water_level = 60.0)
			st.taper(100,max_length=tap_len)
			st.filter('bandpass',freqmin=f_prefilt[0],freqmax=f_prefilt[1],zerophase=True)
			st.trim(starttime=date1,endtime=date2,fill_value=0,pad=True)
			st.interpolate(newfreq,starttime=date1+tap_len,npts=int(((date2-tap_len)-(date1+tap_len))*newfreq),method='cubic')


		else:
			st.remove_response(output="VEL",taper = False ,water_level = 60.0)
			st.taper(100,max_length=tap_len)
			st.filter('bandpass',freqmin=f_prefilt[0],freqmax=f_prefilt[1],zerophase=True)
			st.trim(starttime=date1,endtime=date2,fill_value=0,pad=True)
			st.interpolate(newfreq,starttime=date1+tap_len,method='cubic',npts=int(((date2-tap_len)-(date1+tap_len))*newfreq))
			
			
		return st

#------------------------------------
def manage_gaps(st,f_prefilt,Ltrace):
	Lgaps=0
	for listOfgGap in st.get_gaps():
		Lgaps = Lgaps+listOfgGap[6]
	if Lgaps < 3*Ltrace/4 and len(st.get_gaps()) < 24 : # stats sur tous les gaps ...
		st2  = st.copy()
		ig   = 0
		while len(st2.get_gaps()):
			if ig > len(st2.get_gaps())-1:
				break
			conditionGAP1  = st2.get_gaps()[ig][6] < 1/f_prefilt[0]
			conditionGAP2  = st2.get_gaps()[ig][6] < 2/f_prefilt[0]
			conditionLEN1  = len(st2[ig])   < (4 / f_prefilt[0]) * st2[ig].stats.sampling_rate
			conditionLEN2  = len(st2[ig+1]) < (4 / f_prefilt[0]) * st2[ig+1].stats.sampling_rate    
			condition      = conditionGAP1 or ((conditionLEN1 or conditionLEN2) and conditionGAP2)
			if condition:
				st3 = Stream(traces=[st2[ig], st2[ig+1]])
				st3.merge(method=1,fill_value='interpolate') ## POSSIBLE SMALL PHASE SHIFT ...
				st2.remove(st2[ig+1])
				st2.remove(st2[ig]) 
				st2 = st2+st3
				st2.sort()
				st3.clear()
			else:
				ig = ig+1
		st2._cleanup()
		st = st2.copy()
		st2.clear()
		for tr in st:
			if len(tr) < (4/f_prefilt[0]) * tr.stats.sampling_rate:
				st.remove(tr) 
	else:
		#dd.dispc('                                    ... gapssss','r','blink')
		st.clear()
	return st

 







		
