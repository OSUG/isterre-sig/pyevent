
def copy_dict_to_group(h5,dict_,prefix=''):
	''' copy the dict dict_ into h5[/prefix/key_list] '''
	for ikey in dict_ :
		h5.create_group(prefix+'/'+ikey)
		for dname, dset in dict_[ikey].iteritems():
			if type(dset)==list and len(dset) > 0 and type(dset[0]) == dict :
				copy_dict_to_group(h5,{dname : dset[0]},prefix='/'+ikey)
			if type(dset) == dict : 
				# replace False by 0 and True by 1 for matlab compatibility :
				copy_dict_to_group(h5,{dname : dset},prefix='/'+ikey)
				##rint 'im a dict'
			else :
				if type(dset) == bool :
					if dset == False :
						dset = 0 
					if dset == True :
						dset = 1
				h5.create_dataset(prefix+'/'+ikey+'/'+dname,data=dset)
	return h5 



def copy_dataset_tree(h5_src,h5_dest,dname) : 
	''' copy : h5_src['/dname/sta1/sta2/*'] to h5_dest if to does not exist'''
	for sta1 in h5_src[dname] :
		for sta2  in h5_src[dname][sta1]:
			dset_root = dname+'/'+sta1+'/'+sta2
			if not dset_root in h5_dest : 
				h5_dest.copy(h5_src[dset_root],dset_root)

