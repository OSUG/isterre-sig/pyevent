import cPickle 

def load(filename) : 
	#dd.dispc('  loading '+filename,'y','n')
	ff=open(filename,'r') 
	db=cPickle.load(ff)
	ff.close() 
	return db 

def save(filename,db) :
	ff=open(filename,'w')
	cPickle.dump(db,ff)
	ff.close()