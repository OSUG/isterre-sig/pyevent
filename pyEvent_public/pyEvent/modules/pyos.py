import os, glob 

def mkdir(dir_name) : 
	if not os.path.isdir(dir_name) : 
		try :
			os.makedirs(dir_name)
		except :
			pass

#---------------------
def remove(filename) : 
	try :
		os.remove(filename)
	except :
		pass 

#----------------------------
def remove_dir(dir_name) : 
	file_list=glob.glob(dir_name+'/*')
	for ifile in file_list  :
		try :
			os.remove(ifile)
		except :
			pass 
	try :
		os.removedirs(dir_name)
	except :
		pass 

def rename(src,dest) :
	try : 
		os.rename(src,dest)
	except 	:
		pass 

