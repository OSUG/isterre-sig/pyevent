#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
:author:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)

:copyright:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)

:license:
  GPL-3.0-or-later

 
"""

import pickle
import urllib
#import urllib2
import numpy as np 
from obspy.core import UTCDateTime, Stream
from obspy.geodetics import locations2degrees


global plotmap 
plotmap = True

try:
  import matplotlib.pyplot as plt
  from matplotlib.transforms import offset_copy
  import cartopy.crs as ccrs
  import cartopy.io.img_tiles as cimgt
  import cartopy.feature as cfeature
  plotmap = True
except:
  pass

import pyEvent.modules.lang as lang
import pyEvent.modules.dd as dd 
#import ipdb


##########################################################################################
##########################################################################################
###########################   EVENT ########################### 
##########################################################################################
##########################################################################################

def find_events(dc,input_user={},eventfile='events'):
    ''' this function check the data availability on is FDSN database 
    It create the *evtfile*.txt file that is needed for the rest of the following processing
        If cartopy and matplotlib are availble, maps are also created (*evtfile*.pdf)
    Exemple of request
    http://service.iris.edu/fdsnws/event/1/query?start=2017-01-01&end=2017-07-31&minmag=5.5&format=text&catalog=NEIC%20PDE
    #EventID | Time | Latitude | Longitude | Depth/km | Author | Catalog | Contributor | ContributorID | MagType | Magnitude | MagAuthor | EventLocationName
  10281716|2017-07-30T21:00:49|46.2103|150.9531|86.71|us|NEIC PDE|us|us2000a2r4|mb|5.7|us|KURIL ISLANDS
  10280684|2017-07-28T02:39:15|54.3029|169.3011|10.0|us|NEIC PDE|us|us2000a21g|Mww|5.5|us|KOMANDORSKIYE OSTROVA REGION
    '''
    values                    = {}
    values['start']             = ''
    values['end']             = '*'
    values['minmag']             = '*'
    values['format']         = 'text'
    #values['catalog']          = 'NEIC PDE'
    values   = lang.merge_options(values,input_user)
    if dc == 'IRIS':
      url      = '   http://service.iris.edu/fdsnws/event/1/query?'
    elif dc == 'INGV':
      url      = '   http://webservices.ingv.it/fdsnws/event/1/query?'

    data     = urllib.parse.urlencode(values)
    #req      = urllib2.Request(url + data)
    s='Requete ='+ str(url+data)
    print(s)
    try:
        #response = urllib2.urlopen(req)
        response = urllib.request.urlopen(url+data)
        
    except:
        dd.dispc('failed to process this request : ' + url + data,'r','n')
        pass

    #p=response.read().split('\n')
    t=response.read().decode('utf-8')
    p=t.split('\n')

    dico={}
    k=1
    while k<=len(p)-2:
 
      line_info = p[k].split('|')
      (number,Evtime,lat,lon,dep,info,cat,country, info2,magtype,mag,info3,place) = p[k].split('|')
      if dc == 'INGV':
        Evtime=Evtime.split('.')[0]
      Ename=Evtime
      dico[Ename]={}
      dico[Ename]['Evtime']  = Evtime
      dico[Ename]['lat'] = lat
      dico[Ename]['lon'] = lon
      dico[Ename]['dep'] = dep
      dico[Ename]['mag'] = mag
      #dico[Ename]['place'] = place
      #dico[Ename]['cat'] = cat
      #print("EVTIME=", dico[Ename]['Evtime'])


      k+=1
    #list_file = ['Evtime','lat','lon','dep','mag','place','cat']
    list_file = ['Evtime','lat','lon','dep','mag']
    num_ = 0
    ff = open(eventfile + '.txt','w')
    for Ename in dico:
      #print("dico", dico[Ename])
      mline = []
      for kk in list_file:
        mline.append(str(dico[Ename][kk]))
        #print mline

      fline    = mline[0]
      #print("fline=",fline)
      for mm in mline[1:]: fline = fline + '    ' + mm
      num_ += 1
      ff.write(fline + '\n')
    ff.close()


    if plotmap:
      flat = []
      flon = []
      lat = []
      lon = []
      for Ename in dico:
        lat.append(float(dico[Ename]['lat']))
        lon.append(float(dico[Ename]['lon']))
 #     ipdb.set_trace()
      mmap(lon,lat,eventfile + '.png',info=url+data) # lat and lon is a docitionnary of latitude longitude of events
      dd.dispc('Successful request ! \n' + url + data,'g','n')
      dd.dispc('number of event(s) : ' + str(num_) ,'c','n')
   
##########################################################################################
##########################################################################################
#########           #####         ####               #####           #####################
#########           ####          ####               ####     ############################
#########     ##########     ##############     #########     ############################
#########     #########     ###############     ##########        ########################
#########        ######     ###############     ##############       #####################
#########     #########      ##############     ##################    ####################
#########     ##########          #########     #################     ####################
#########     ###########         #########     ##########           #####################
##########################################################################################
##########################################################################################



#---------------------------------------------------------------------------------------
#-------------------- STAIONS SUBFUNCTIONSFUNCTIONS ----------------------------------
#---------------------------------------------------------------------------------------


def mmap(lon=[2.39,5.72],lat=[47.08,45.18],filename='stations.png',info='station map'): # map will be drawn over the whole world or centered around the events - dl
    stamen_terrain = cimgt.StamenTerrain()
    ax = plt.axes(projection=stamen_terrain.crs)
    minlat = -90 
    maxlat = 90
    minlon = -180
    maxlon = 180
    dl     = 10 # in degrees
    if min(lat) - dl > minlat: minlat = min(lat) - dl
    if max(lat) + dl < maxlat: maxlat = max(lat) + dl
    if min(lon) - dl > minlon: minlon = min(lon) - dl
    if max(lon) + dl < maxlon: maxlon = max(lon) + dl
    ax.set_extent([minlon, maxlon, minlat, maxlat])
    r = int(locations2degrees(minlat,minlon,maxlat,maxlon)*111)
    if r <= 1000 :
        res = 8 
        ax.coastlines('10m')
    if r > 1000 and r <= 5000:
        res = 6
        ax.coastlines('50m')    
    if r > 5000 and r <= 10000: 
        res = 4
        ax.coastlines('110m')
    if r > 10000: 
        res = 2
        ax.coastlines('110m')
    ax.add_image(stamen_terrain,res)
    ax.gridlines(draw_labels=True)
    plt.plot(lon,lat, "o", color='red', markersize=5,transform=ccrs.Geodetic())
    plt.savefig(filename,dpi=None, facecolor='w', edgecolor='w',
        orientation='portrait', papertype=None, format=None,
        transparent=False, bbox_inches='tight', pad_inches=0.1,
        frameon=None)
    #plt.show()
    plt.close()









   

 
    



