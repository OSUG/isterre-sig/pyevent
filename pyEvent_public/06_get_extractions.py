#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
:author:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)

:copyright:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)

:license:
	GPL-3.0-or-later

Parameters file for the events extraction program. Events will be dowloaded, processed and write in the chosen file format.

Exemple:
	06_get_extractions.py
 
"""



import pyEvent.m60_extractions as extractions
from obspy.core import UTCDateTime
in_                   = {}
sta_                  = {}
evt_                  = {}

###############
# Standard - do not forget to create the data_path and log_path before launching the program
###############

in_['ch']             = ['BH','HH']# ['BH','HH'] It will try in this order... better to start from the lower sampling rate
in_['cmp']            = ['Z','N','E'] #'Z' or ['cc1','cc2','cc3']
in_['data_path']      = '' #Users/DATA
in_['log_path']       = '' #Users/DATA/log
in_['nodatafile']     = '' #Users/DATA/log/nodata
in_['mode']           = 1  # mode=0 initial mode - will not attempt to fill event directory if new stations added to the list
						   # mode=1 completion mode - will attempt to fill event directory when new stations added to the list - extraction corresponding to the station will be searched in th epkl file


############################
# preprocessing and download
############################ 
in_['pp']                = dict() # always 'dict()'
in_['pp']['tr_len']      = 60*60 # [s] length of the trace - minimum 10*60 sec
in_['pp']['tap_len']     = 10*60 # [s] dwld date1-tap_len -> date1+tr_len+tap_len 
in_['pp']['pre_event']   = 10*60  # [s] dwld date1-pre_event-tap_len -> date1-pre_event+tr_len+tap_len (start time of extracted trace is date1(event start time) - pre_event)
in_['pp']['freq']        = 10. # freq. to which data are decimated [hz] => output sampling rate 1.
in_['pp']['f_prefilt']   = (0.008, in_['pp']['freq'] / 2 - 0.05 * in_['pp']['freq']) # prefilt before sensor reponse deconvolution, and after decimation
in_['pp']['remove_resp'] = True # True or False. If False, it will try to download resp from clients but DO NOT try to read from SDS archive
in_['pp']['data_output'] = 'SAC' # SAC, MINISEED, HDF5

###### FDSNWS protocol with login/pwd - if no login and pwd leave the fields blank. If only 1 network requires fdsnws login and password, fill it###########
in_['pp']['login']       = '' # login for fdsn ws client (Z3-Fr and XT data)
in_['pp']['pwd']         = '' # pwd for fdsn ws client (Z3-Fr and XT data)
in_['pp']['net_fdsnws'] = [('XT','RESIF')] # list of tuples (network code, datacenters) using fdsnws protocol with a login and pwd

###### EIDA TOKEN authentication system for fdsnws - FILL THIS PART ###########
in_['pp']['eida_token'] = '' # /Users/EidaToken/ EIDA token maximum validity = 1 month. EIDA token request at https://geofon.gfz-potsdam.de/eas/ 
in_['pp']['net_token'] = [('Z3','LMU'),('Z3','ORFEUS'),('Z3','GFZ'),('Z3','RESIF'),('Z3','ETH'),('C4','ETH')] # list of tuples (network code, datacenters) 

# ...
stafile             = 'stations' # stations file
evtfile             = '/Users/aubercor/Documents/PROCESSING/pyEvent_public/event' # evt output name (.txt and .png)
############### RUN
sta_,nbsta = extractions.read_station_list(stationfile=stafile)
evt_ = extractions.read_event_list(eventfile=evtfile)
extractions.download_extractions(nbsta,input_user=in_,sta_list=sta_,evt_list=evt_)

