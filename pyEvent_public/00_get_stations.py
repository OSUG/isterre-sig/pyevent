import pyEvent.m00_get_stations as get_sta
from obspy.core import UTCDateTime
in_                   = {}

###############
# Standard
###############  
# Here you'll find all possible options and their format 
# =====>    http://service.iris.edu/irisws/fedcatalog/1/
#in_['key']        = value 

#in_['net']             = 'GE,FR,RD,G,CA,OE,NL,BE,II,ES,GR,TH,GU,IV,MN,CH'
in_['net']             = 'CH' # 'net1,net2,net2...' accept wildcard
in_['sta']             = '*' # 'sta1,sta2,sta3...' accept wildcard
in_['loc']             = '*' # 'loc1,loc2,loc3...' accept wildcard
in_['channel']         = '*HZ' # 'ch1,ch2,ch3...' accept wildcard
in_['start']           = '2015-01-01' # yyyy-mm-dd
in_['end']             = '2019-09-05' # yyyy-mm-dd
in_['minlat']          = '10' # min lat
in_['maxlat']          = '80' # max lat
in_['minlon']          = '1' # min lon
in_['maxlon']          = '50' # max lon
in_['includeavailability'] = True
# ...

stafile                = 'stations' # output name (.txt and .png)

############### RUN
get_sta.find_stations(input_user=in_,stafile=stafile)

